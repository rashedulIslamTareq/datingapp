﻿using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using DatingApp.API.Data;
using DatingApp.API.Models;


namespace DatingApp.API.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private readonly DataContext _context;

        public ValuesController(DataContext context)
        {
            _context = context;
        }


        // GET api/values
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var results = await _context.Values.ToListAsync();
            return Ok(results);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var value = await _context.Values.FirstOrDefaultAsync(x => x.Id == id);
            return Ok(value);
        }

        // POST api/values
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Value value)
        {
            await _context.Values.AddAsync(value);
            var result = await _context.SaveChangesAsync();

            return Ok(result > 0);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody]Value value)
        {
            var valueToUpdate = await _context.Values.FirstOrDefaultAsync(x => x.Id == id);
            valueToUpdate.Name = value.Name;
            var result = await _context.SaveChangesAsync();

            return Ok(result > 0);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var value = await _context.Values.FirstOrDefaultAsync(x => x.Id == id);
            await Task.Run(() => _context.Values.Remove(value));
            var result = await _context.SaveChangesAsync();

            return Ok(result > 0);
        }
    }
}
